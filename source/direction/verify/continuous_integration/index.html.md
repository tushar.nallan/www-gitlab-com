---
layout: markdown_page
title: "Category Vision - Continuous Integration"
---

- TOC
{:toc}

## Continuous Integration

Many teams report that CI, while important and commonly adopted, is too slow and teams are unsure how to speed it up. It's often a black box that few people understand and diving in feels like an interruption to the more important work of delivering features. This presents a unique opportunity for CI solutions that make optimization of the pipeline easier and more automatic.

While we are very proud that GitLab CI/CD was recognized as [the leading CI/CD tool on the market](https://about.gitlab.com/2017/09/27/gitlab-leader-continuous-integration-forrester-wave/), and has been called `lovable` by many, many users - we still have a vision for how we can make what we've built even better.  In other epics, we talk about [Speed & Scalability](https://gitlab.com/groups/gitlab-org/-/epics/786) and [making CI lovable for every use case](https://gitlab.com/groups/gitlab-org/-/epics/811).  This vision epic will cover the broadest area of CI in GitLab - and how we're going to go from `lovable` to `undeniably amazing`. 

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=continuous%20integration&sort=milestone)
- [Overall Vision](https://about.gitlab.com/direction/verify)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/592)

### Continuous Integration vs. Delivery vs. Deployment

CI and CD are very closely related areas at GitLab: our CI platform (what we call Verify) provides the foundation for CD and is the [Gateway to Operations](https://about.gitlab.com/direction/verify/#gateway-to-operations).  In fact, Verify is also the first stage in our [CI/CD sub-department](https://about.gitlab.com/handbook/product/categories/#cicd-sub-department) which also includes Package and Release.  In addition to the vision for each of those stages, we maintain a sub-department wide [vision for CI/CD](https://about.gitlab.com/direction/cicd/)

### GitLab Runner

The GitLab Runner is managed by the Verify team also, and is a key component of the CI solution. You can find the vision epic for the Runner at [gitlab-org#573](https://gitlab.com/groups/gitlab-org/-/epics/573).

## What's Next & Why

Up next are a few MVC epics and issues to help advance our 2019 Product Vision:

* Windows Container Executor [gitlab-org#535](https://gitlab.com/groups/gitlab-org/-/epics/535)
* Vault integration as part of [gitlab-org#816](https://gitlab.com/groups/gitlab-org/-/epics/816)
* Cross-project `triggered-by` to allow downstream pipelines to watch upstream pipelines for changes [gitlab-ee#9045](https://gitlab.com/gitlab-org/gitlab-ee/issues/9045)

They may seem like small, deliberate steps forward, but this is intentional as we both want to iteratively see how user behavior evolves as we deliver these features, as well as make deep architectural changes within the product implementation at a deliberate pace.

## Competitive Landscape

### CloudBees Jenkins/CodeShip

Jenkins has been for a while the punching bag for how you sell DevOps software (just saying "my product solves the same problems as Jenkins, but without the huge expensive legacy mess" have given you the keys to the kingdom.) Jenkins is trying to address this by [splitting](https://jenkins.io/blog/2018/08/31/shifting-gears/) their product into multiple offerings, giving them the freedom to shed some technical and product debt and try to innovate again. They also acquired CodeShip, a SaaS solution for CI/CD so that they can play in the SaaS space. All of this creates a complicated message that doesn't seem to be resonating with analysts or customers yet.

At the moment there isn't a lot that needs to be done here to maintain our lead over the product, but continuing to add product depth (that does not add complexity) via features like [gitlab-ee#2122](https://gitlab.com/gitlab-org/gitlab-ee/issues/2122) will allow us to clearly demonstrate that our product is as mature as Jenkins but without the headache.

### Azure DevOps/GitHub

Microsoft is planning to use their GitHub acquisition to pick up market share in going after open source software development for their new integrated solution. There had been a trend to move away from them because users saw their current development tools as old-fashioned; with rebranding them to Azure Devops (and investment in improving their capabilities) they are trying to disrupt this decision by their customers.

Additionally, GitHub actions has introduced a new kind of system for CI that offers a marketplace of snippets that can be contributed, linked together in easy ways including to systems traditionally outside of the scope of CI, and build pipelines for delivery. It's an interesting use case for straightforward use cases, and aligns to the decentralized model that software development is moving towards, but also could end up being quite complex and very hard to predict exactly what the system will do after everything is wired together.

To compete against GitHub Actions, we are looking at introducing directed acyclic graphs in the pipeline via [gitlab-ce#47063](https://gitlab.com/gitlab-org/gitlab-ce/issues/47063), which will allow better performance within the pipeline by managing the dependencies directly instead of grouping things per stage. This is a marketable improvement that can be used to demonstrate parity with GitHub Actions.

### Bitbucket Pipelines and Pipes
BitBucket pipelines has been Atlassian's answer to a more CI/CD approach than the traditional Atlassian Bamboo that is very UI driven builds and deploys by allowing users to create yml based builds.

On February 28, 2019, Atlassian announced [Bitbucket Pipes](https://bitbucket.org/blog/meet-bitbucket-pipes-30-ways-to-automate-your-ci-cd-pipeline) as an evolution of Bitbucket pipelines and gained a lot of press around [Atlassian taking on GitHub/Microsoft](https://www.businessinsider.com/atlassian-bitbucket-pipes-ci-cd-gitlab-github-2019-2).  While it is early in the evolution of this as a product in the enterprise market, there are a number of interesting patterns in the announcement that favor Convention over Configuration.  The feature that most closely matches this in our roadmap is [gitlab-ce#53307](https://gitlab.com/gitlab-org/gitlab-ce/issues/53307).

## Analyst Landscape

There are a few key findings from the Forrester Research analysts on our CI solution:

- GitLab is seen as the best end to end leader where other products are not keeping up and not providing a comprehensive solution. We should continue to build a deep solution here in order to stay ahead of competitor's solutions.
- Competitors claim that GitLab can go down and doesn't scale, which is perhaps their best argument if their products do not provide as comprehensive solutions. This isn't really true, so we need to have messaging and product features that make this clear.
- In general, cloud adoption of CI/CD is growing and our CI needs to be ready.

To continue to drive in this area, we are considering [gitlab-ce#40720](https://gitlab.com/gitlab-org/gitlab-ce/issues/40720) next to add Vault integration throughout the CI pipeline. This builds off of work happening on the Configure team and will allow for a more mature delivery approach that takes advantage of ephemeral credentials to ensure a rock solid secure pipeline.

## Top Customer Success/Sales Issue(s)

A very popular issue driving customer contacts is the ability to clean up old container revisions ([gitlab-ce#25322](https://gitlab.com/gitlab-org/gitlab-ce/issues/25322)). This is a complicated problem, but one that causes a lot of disk space usage in certain scenarios.

## Top Customer Issue(s)

Our most popular customer item by far is [gitlab-ce#23902](https://gitlab.com/gitlab-org/gitlab-ce/issues/23902), pipelines for merge requests. Work moving us towards this goal was begin in our 2018 product vision, and will continue into the one for 2019.

## Top Internal Customer Issue(s)

The issue about `triggered-by` functionality is currently the top internal customer issue: [gitlab-ee#9045](https://gitlab.com/gitlab-org/gitlab-ee/issues/9045).  Other top internal customer issues include:

* Adding generic 'metrics' report type to merge requests [gitlab-ee#9788](https://gitlab.com/gitlab-org/gitlab-ee/issues/9788)
* Adding only/expect rule for merge request pipelines [gitlab-ee#9598](https://gitlab.com/gitlab-org/gitlab-ee/issues/9598)
* Showing a high visiblity alert if master is red [gitlab-ee#10216](https://gitlab.com/gitlab-org/gitlab-ee/issues/10216)

## Top Vision Item(s)

Our most important vision item is [gitlab-org#414](https://gitlab.com/groups/gitlab-org/-/epics/414) to allow first-class support for all cross-project triggering workflows, followed closely by Directed acyclic graphs (DAG) for pipelines MVC [gitlab-ce#47063](https://gitlab.com/gitlab-org/gitlab-ce/issues/47063).