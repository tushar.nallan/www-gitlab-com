---
layout: markdown_page
title: "Diversity and Inclusion"
---

![Our Global Team](/images/summits/2017_mexico_team.jpg){: .illustration}*<small>In January 2017, our team of 150 GitLabbers from around the world!</small>*

### Diversity and Inclusion at GitLab

Over 100,000 organizations utilize GitLab across the globe and we aim to have a team that is representative of our users. 

Diversity and Inclusion are fundamental to the success of GitLab. We aim to make a significant impact in the our efforts to foster an environment where everyone can thrive. We are designing a multidimensional approach to ensure that GitLab is a place where people from every background and circumstance feel like they belong and contribute. Collaborative work is one major component of our work, we truly aim to uphold a culture which embodies transparency, opportunity and open communication. 

### Values

Inclusive teams are naturally more engaged, collaborative and innovative. We aim to align our [our values](/handbook/values/) to be reflective of our company wide commitment to fostering a diverse and inclusive environment. 

In addition, the very nature of our company is to facilitate and foster inclusion. We believe in asynchronous communication, we allow flexible work hours. Gitlabber’s are encouraged to work when and where they are most comfortable. 

### Fully Distributed and Completed Connected

The GitLab team is fully distributed across the globe, providing our team the opportunity to connect with each others cultures, celebrations and unique traditions. We collaborate professionally and connect personally! 
Our unique remote team opens our door to everyone. Candidates are not limited by geography and we [champion this approach](http://www.remoteonly.org/), to the extent that it’s possible, for all companies! 

### GitLabber Data

Please see our [identity data](/company/culture/inclusion/identity-data)

### Inclusion at GitLab

Gitlab is growing significantly, making it imperative to continuously foster community and build allyship. Diversity and Inclusion are crucial to building a collaborative company wide culture. We aim to retain all great talent. 

GitLab Best Practices:

*  Inclusive interviewing - We are building a an inclusive workforce to support every demographic. One major component is ensuring our hiring team is fully equipped with the skills necessary to connect with candidates from every background, circumstance. We thrive to ensure our hiring team is well versed in every aspect of Diversity, Inclusion and Cultural competence. We are helping the unconscious become conscious. Our number one priority is a comfortable and positive candidate experience. 

*  Inclusive benefits - We list our [Transgender Medical Services](/handbook/benefits/#transgender-medical-services) and [Pregnancy & Maternity Care](/handbook/benefits/#pregnancy--maternity-care) publicly so people don't have to ask for them during interviews.

*  Inclusive language - In our [general guidelines](/handbook/general-guidelines/) we list: 'Use inclusive language. For example, don't use "Hi guys" but use "Hi everybody", "Hi people", or "Y'all". And speak about courage instead of [aggression](https://www.huffingtonpost.com/2015/06/02/textio-unitive-bias-software_n_7493624.html). Also see the note in the [management section of the leadership page](/handbook/leadership/#management-group) to avoid military analogies.

#### Unconscious bias training

To be truly inclusive is to be aware of your own ingrained biases as well as strategies for blunting the effects of those biases. As part of our inclusivity efforts we recommend everyone to partake in [the Harvard project Implicit test](https://implicit.harvard.edu/implicit/takeatest.html) which focusses in on the hidden causes of everyday discrimination.

Diversity and Inclusion Goals: 

*  Empower employees with Employee Resource Groups
*  Ensuring equal access to opportunities 
*  Create Diversity Team - A team of company influencers who can be instrumental in driving D&I efforts. 
*  GitLab Safe Spaces - team wide opportunities for GitLabber's to share perspectives, concerns and differnt outlooks. 

### Employee Training and learning opportunities

*  Inclusive interviewing

*  Coaching for inclusion 

*  Conflict resolution

*  Understanding Unconscious bias 

*  Psychological workplace safety 

### Community

GitLabbers are distributed across the globe, giving us access to an array of opportunity. We encourage collaboration with global organizations and programs that support underrepresented individuals in the tech industry. GitLab also provides additional support through the Diversity Sponsorship program. [GitLab Diversity Sponsorship program](/community/sponsorship/). We offer funds to help support the event financially and, if the event is in a city we have a GitLab team member, we get hands-on by offering to coach and/or give a talk whenever possible.

### Mentorship

Gitlabbers can benefit from a [mentoring program](https://docs.google.com/forms/d/e/1FAIpQLSc68PSMge0iVoDoVU19n0JycHMMkVpq55gXj59ykdJJkcM_rg/viewform) because they will contribute to the development of a better-trained and engaged community.
Mentors will help mentees learn the ropes at the company, develop relationships across the organization and identify skills that could be strengthened.

#### Additional Training Opportunities 

[Salesforce Equality at Work Training](https://trailhead.salesforce.com/trails/champion_workplace_equality).
 To earn badges and save your responses, you'll need to sign up! Use your GitLab address to sign in using Google+.
   * [Business Value of Equality](https://trailhead.salesforce.com/trails/champion_workplace_equality/modules/workplace_equality_diversity_and_inclusion) (This module has three units. The third is specific to Salesforce values and mission and is not required or suggested for our training.)
   * [Impact of Unconscious Bias](https://trailhead.salesforce.com/en/trails/champion_workplace_equality/modules/workplace_equality_inclusion_challenges)
   * [Equality Ally Strategies](https://trailhead.salesforce.com/trails/champion_workplace_equality/modules/workplace_equality_ally_strategies)
   * [Inclusive Leadership Practices](https://trailhead.salesforce.com/trails/champion_workplace_equality/modules/inclusive-leadership-practices)

### Never done

We recognize that having an inclusive organization is never done. If you work at GitLab please join our #inclusion chat channel. If you don't work for us please email sgarza@gitlab.com with suggestions and concerns.
