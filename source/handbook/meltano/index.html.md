---
layout: markdown_page
title: "Meltano"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Summary

[Meltano](https://gitlab.com/meltano/meltano) is a convention-over-configuration framework for analytics, business intelligence, and data science. It leverages open source software and software development best practices including version control, CI, CD, and review apps.

[View our roadmap](https://meltano.com/docs/roadmap.html)

[File a new issue](https://gitlab.com/meltano/meltano/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=&issuable_template=bugs)

## Team

* [Danielle](https://gitlab.com/dmor) - General Manager
* [Jacob](https://gitlab.com/jschatz1) - Staff Engineer
* [Micaël](https://gitlab.com/mbergeron) - Engineer
* [Yannis](https://gitlab.com/iroussos) - Sr. Engineer
* [Derek](https://gitlab.com/derek-knox) - Sr. Frontend Engineer
* [Ben](https://gitlab.com/bencodezen) - Sr. Frontend Engineer
* [Virjinia](https://gitlab.com/valexieva) - Sr. Product Manager

## Community

We believe in building in public, and you can follow along with our progress:
* [Meltano blog](https://meltano.com/blog/)
* [Meltano YouTube channel](https://www.youtube.com/channel/UCmp7zJAZEC7I_n9BEydH8XQ?view_as=subscriber)
* [Meltano on Twitter](https://twitter.com/meltanodata)
* [Meltano community Slack](https://meltano.slack.com)

## Code

See the active project here: [gitlab.com/meltano/meltano](https://gitlab.com/meltano/meltano)

Read our documentation here: [https://meltano.com/docs/](https://meltano.com/docs/)

## Releases

Meltano is released weekly on Mondays, and follows our [documented release procedure](https://meltano.com/docs/contributing.html#releases)