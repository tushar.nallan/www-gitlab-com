---
layout: markdown_page
title: "Merchandise workflow"
---

## On this page
{:.no_toc}

- TOC
{:toc}

----

## Overview

## Workflow

## Best practices

### Choosing a vendor

As a general rule, consider using [Stickermule](https://www.stickermule.com) for sending stickers, since the Sendoso inventory is limited. If Stickermule doesn't work for you, then use Sendoso instead.

If the merch shipment includes:
* only stickers, always use Stickermule
* a small number of items (depending on Sendoso inventory), use Sendoso
* a large amount of stickers and other merch, consider using both Stickermule and Sendoso

<i class="fas fa-hand-point-right" aria-hidden="true" style="color: rgb(138, 109, 59)
;"></i> Always check the Sendoso inventory and item availability before sending.
{: .alert .alert-warning}

## Automation

We use Shopify's Collections to automate the specific merchandise requests/campaings shipping to the users, event participants or customers.

Shopify's Collections allow us to create a specific link and a dedicated page with items we want to offer. In order to make the items free for the user, you will need to assign a Discount Code to that Collection. This results in the user just have to open the collection link, pick the items and enter their shipping address. At the checkout page, the code should be auto-included so the price and shipping are 0.

### Shopify Collections

#### Creating the Collection

1. From your Shopify admin home page, go to **Products** > **Collections**.
2. Click **Create collection**.
3. Enter a title and description for the collection.
4. In the **Collection type** section, click **Manual**.
5. Click **Save**.
6. In the **Products** section, search for products or click **Browse**, and then add the products that you want to have in the collection.
7. In the **Collection image** section, click **Choose image** to upload an image for the collection.
8. Click **Save**.

<i class="fas fa-info-circle" aria-hidden="true" style="color: rgb(49, 112, 143)
;"></i> For more information, see this [official guide](https://help.shopify.com/en/manual/products/collections/manual-shopify-collection)
{: .alert .alert-info}

#### Creating and assigning the Discount Code to a Collection

1. From your Shopify admin home page, go to **Discounts**.
2. From the **Discounts** page, click **Create discount**.
3. In the **Discount code** section, enter a name for the a discount code (you can enter the same name as the Collection name without spaces or check the existing codes for an example).
4. In the **Options** section, select the fixed discount type.
5. Enter a monetary value for the discount in the **Discount value** box.
6. In the **Applies to** section, select what this discount will apply to: **Specific collections**
7. Use the **search field** or the **Browse button** to add the collection them to the discount.
8. In the **Customer eligibility** section, select who this discount will apply to: **Everyone**, **Specific groups of customers**, or **Specific customers** depending on what is the most convenient for you.
9. The final step is to limit the discount usage, check both options in the **Usage limits** section:

     * **Limit number of times this discount can be used in total** lets you set a total number of times a discount can be used. For example, setting a limit of 200 allows the discount code to be used 200 times in total.
      * **Limit to one per customer** tracks customer emails to limit discount use to one per customer.
10. Click **Save discount**.

When you complete the code setup, the code should be sucessfully assigned to the collection. The only step left is to click the **Promote** button in order to copy the **shareable link of the collection**.

#### Abusive usage of collection links

If anyone reports that the discount code is not auto-included at the checkout page, that may be suspicious (this may happen if the user already redeemed the prize). The user can enter the discount code manually - please ping the Community Advocacy Swag Expert in order to figure out the solution.

The solution is to track who included the discount codes in the Shopify order summary - you'll just need the name and email of the eligible users. If the order and discount code is placed from non-eligible users please cancel the fulfillment via Sendoso's Analytics tab.

### https://shop.gitlab.com/

The external orders received at our [online store](https://shop.gitlab.com/) are automatically fulfilled by [Sendoso](https://sendoso.com/). You can find more details about these orders in the Analytics tab in Sendoso.

#### Canceling any external order on Shopify and Sendoso

You can always cancel the pending/processing orders. All the orders including the orders via Shopify Collections using Discount Codes can be canceled.

1. From your Sendoso home page go to the **Analytics** tab.
2. Search for the order you want to cancel.
3. Click the big **Cancel** on the right side of your order details.
4. You are done. The order won't be fulfilled by Sendoso.

Please note that you should always change the status of the orders in Shopify as well.

1. From your Shopify admin page go to the **Orders** page
2. Click on the order you want to change.
3. Press **More Actions** button in order to see the drop menu options.
4. Select **Cancel order** option.

### External Shopify orders

All the orders received via [shop.gitlab.com](https://shop.gitlab.com/) are automatically forwarded from Shopify to Sendoso via the Sendoso-Shopify integration. Sendoso automatically processes the orders and fulfills the shipments. You can always check the status of the orders in the Analytics tab in Sendoso - don't forget to filter the Analytics with changing the  "--All touches--" to "shop.gitlab.com" for a better breakdown of the Shopify-only fulfillments.

#### The manual part of the workflow

Since the orders from Shopify are auto-forwarded and fulfilled by Sendoso, the only thing left to do is to fulfill the orders in Shopify as well:

1. Open the Shopify **Orders** page.
2. Open the Sendoso Analytics and filter it with **shop.gitlab.com** touch.
3. Compare if all the **Unfulfilled** orders from Shopify are listed in Sendoso Analytics tab.
4. Mark the **Unfulfilled** orders in Shopify.
5. Click drop-down **Actions** button.
6. Choose **Fulfill order** option.

#### Sendoso fulfillment notifications

The updates about the external orders are sent to <merch@gitlab.com>. You can find those messages in the Merchandise view on GitLab Community Zendesk instance.

There are 2 types of notifications:
* **The item has been shipped** - includes the tracking ID.
Example: "Woohoo! The shop.gitlab.com you sent to John Smith has shipped!". shop.gitlab.com highlights that this order was received via Shopify.
* **The user received the merchandise**.
Example: "Woohoo! The shop.gitlab.com you sent to John Smith was delivered!

Feel free to always **Submit as Solved** these notifications in **Zendesk**.

#### Tracking IDs

The Tracking ID is usually assigned by Sendoso 2-3 days after the order is received. Sometimes, users may request their Tracking ID.

Please follow these steps if the user requests the Tracking ID:
1. Look for the requester's **name/email**.
2. Go to the Sendoso **Analytics** page.
3. Search for the order using the name/email.
4. Copy the **Tracking ID**.
5. Confirm that order's details match the requester. You can double check this via Shopify:
    * Search for the same order/person in Shopify
    * Compare if the **items**, **full name**, **email** and the **dates** are correct
6. Email the **Tracking ID** or the full **Tracking Link** to the requester.
    * You can always open the full **Tracking Link** by clicking on the **Tracking ID**.
7. (Optional step) Assign the **Tracking ID** to the requester's order in Shopify:
    * Find the order in Shopify
    * If you already fulfilled the order in Shopify, click **Add tracking** button and paste the ID.
    * If the order is unfulfilled in Shopify, mark it as **fulfilled** and then add the **Tracking ID**.

#### Sending touches via Sendoso 

##### How to create a touch

1. Log in and click on the **Touches** tab
2. Click on the **+ Create New Touch** button
3. Select the touch from a variety of options and click **Next steps** button
    * If you want to choose our merchandise, select **BYO box** under the **Inventoried Sends** section
    * Note: any physical item must be sent to our warehouse prior to creating the touch
4. Enter the 3 required details and click **Next steps** button:
    * Touch name for the sender (e.g. Send All) 
    * Select warehouse
    * Search the product/s
5. Change the **Charge Cost to** field to Funding Source
6. Pick the **Community Relations** source in the drop down menu below.
7. Under the section **Shipping method** enable the **Allow Senders to Select Shipping method** option and click **Next steps**
8. Click **Finish** button

##### Send touch

1. Click on the **Send** tab
2. Click on the touch you'd like to send
3. Pick the items and enter the **quantity** and click **Next** button
4. Select **To a single person/company** and fill in the shipping info below
5. Please consider the following shipping options:
    * For the regular shipments, pick **Optimized sendoso shipping**
    * For expedited shipping, please choose between the **2/3 days** or **Overnight** shipping options
    * Note that the expedited shipping works only in the US

### Add or remove the products from Shopify

#### Adding items

1. Gather item inventory data - contact the product's vendor
1. Log in to Shopify
1. Open the products page
   - Click the Add Product button
   - Fill out information about the item
   - If you don't have the information for the description, please ask/search for it and be careful - the info could be sensitive
   - Image is important, contact the product vendor for the high-rez photo.
   - Fill out the price for the item
   - Select "Shopify tracks this product's inventory"
   - Enter the weight of the product if that info is available.
   - Before saving the product, please check search engine listing preview

<i class="fas fa-info-circle" aria-hidden="true" style="color: rgb(49, 112, 143)
;"></i> For more information, see this [official guide](https://help.shopify.com/en/manual/products/add-update-products)
{: .alert .alert-info}

#### Removing items

1. Log in to Shopify
1. Open the Products page
   - Click on the product you want to remove
   - Scroll to the bottom of the page where you can find the delete button

<i class="fas fa-info-circle" aria-hidden="true" style="color: rgb(49, 112, 143)
;"></i> For more information, see this [offical guide](http://shopifynation.com/shopify-tutorials/delete-products-variants-shopify/)
{: .alert .alert-info}

## Handling swag

### MVP Appreciation Gifts

Each 22nd of the month is a release day - every release we pick a Most Valuable Person and thank them for their contributions. We send them some GitLab swag as a thank you (e.g. a hoodie, socks, and a handmade tanuki). There's also the option of sending personalized swag - see [custom swag providers](#good-custom-swag-providers).

1. Determine MVP after merge window closes, see `#release-post` channel
1. Find MVP's contact information
  * An email address is usually stored in git commit data
  * A user might have email or twitter info on their profile
1. Congratulate the MVP via email, ask for their shipping address, as well as any other relevant information (e.g. shirt size)
1. Investigate the MVP's interests
  * If the MVP doesn't have a notable presence on social media, you may choose to ask them directly or send GitLab swag instead
1. Choose a suitable gift (up to 200$ USD)
1. Write a kind thank you message
1. Send the gift
  * The MVP should ideally have the gift 48h before the post goes live, though shipping to people outside the United States can take longer and usually won't make it in time
1. Verify shipment status
  * Make sure that it was sent
  * Make sure that it arrived
1. Mention the MVP gift in the release post
  * Make sure there's a picture of the gift in the release post if it's available

### Handling #swag channel and <merch@gitlab.com> requests

#### Internal GitLab Merchandise request

Everyone can request merch in the #swag Slack Channel and ping the swag expert (dsumenkovic) or email the request to <merch@gitlab.com>.

We can ship the packge to any location:
* If you are attending an event, you can request the merch to arrive at your place.
* If you want to send gift to the customer, user or contributor - you can request to arrive at your location so you can hand it personally or we can send it directly to the recipient.

Please include the following info for any type of request:
* What merchandise items do you need.
* The amount of merch needed - feel free to ask the Community Advocate swag expert for the suggestion if you are not sure.
* The merchandise shipping address and contact phone number - feel free to continue the conversation via DM if you don't want to share this info publicly.
* If you are giving away a swag gift to a contributor please include the URL to a blog post, Tweet or the contribution.

Note: we recommend that you request merchandise at least 4 weeks in advance for us to be able to accommodate your request. However,
* If your request is urgent, please reach out to the swag expert and find out if the fast shipping option is available.
* Feel free to schedule a Zoom call with the swag expert to discuss, create and place the order.

#### Community Advocates
* Make sure you regularly check the #swag channel and the Merchandise view in the Zendesk.
* If the recipient is  a contributor, user or customer make sure you reach out to the recipient via <community@gitlab.com>:
  * Thank them for their work/support
  * Gather the missing info needed for fulfilling the swag dropship if needed
* Fulfill the shipment in Sendoso:
  * Go to the Send tab and pick the items you want
  * Add the items amount
  * Fill shipment info
  * Pick the shipping method
  * Click Send to complete the order

Note: we recommend you to pick the Optimized Sendoso shipping method. If you need to ship the package as fast as possible, feel free to choose between Overnight, Two Days or Three Days shipping methods (only for the US locations).

<i class="fas fa-hand-point-right" aria-hidden="true" style="color: rgb(138, 109, 59);"></i> Please bear in mind the [list of countries we do not do business in](/handbook/sales/#export-control-classification-and-countries-we-do-not-do-business-in).
{: .alert .alert-warning}

### Delayed and lost merchandise shipments

From time to time it may happen that the package never arrives to the customers. Customers usually complain via <merch@gitlab.com>, however, keep an eye on Twitter, the #swag Slack channel and other related threads.

Please check if the package is still in transport using the tracking ID and reach out to the customer with brief details.

If the package has been in transport over 2-3 weeks, consider apologizing and refunding the 20% of the whole order using Shopify's refund option:
1. Login to Shopify.
2. Search the order by name/email/orderID.
3. Select the order.
4. Select the "Refund items" option.
5. On the right part of the page, you have the fields to enter the custom value and reason for a refund.
6. If you are not sure how to calculate the 20%, multiply 0.2 with the whole amount and that's the exact value.
7. Use "Reason for refund" field and write the appropriate message explaining that we are refunding 20% of the whole amount due to delayed shipping and press the Refund button.
8. Don't forget to apologize to the customer using the original thread (e.g. respond via the original Zendesk ticket) and offer any other assistance if needed.

If the customer complains that the package never arrived and the package status is "completed" or "delivered", consider the following options:
1. Reach out to the vendor (<support@sendoso.com> or <help@stickermule.com>) and ask if they have information about that specific order.
2. If the package has been returned or lost, consider asking them to resend it.
3. If the vendor doesn't resend the package, do it manually asap.
4. Always consider refunding the whole order amount to the customer.
5. Since we care about our community and customers, feel free to include extra item/s of your choice, create a coupon code for an apology or any other idea. In this case, the main goal is to make the customer happy.
6. Don't forget to coordinate everything with the customer (use <merch@gitlab.com> for conversation), apologize and find out if there's any other thing we could do for them.
