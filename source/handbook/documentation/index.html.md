---
layout: markdown_page
title: Documentation
---

GitLab’s documentation is crafted to help users, admins, and decision-makers learn about GitLab features and to optimally implement and use GitLab to meet their [DevOps needs](https://about.gitlab.com/stages-devops-lifecycle/). 

The documentation is an essential part of the product. Its source is developed and stored with the product in its respective paths within the GitLab [CE](https://gitlab.com/gitlab-org/gitlab-ce/tree/master/doc), [EE]((https://gitlab.com/gitlab-org/gitlab-ee/tree/master/doc)), [Runner](https://gitlab.com/gitlab-org/gitlab-runner/tree/master/docs), and [Omnibus](https://gitlab.com/gitlab-org/gitlab-ee/tree/master/doc) repositories. It is published at [docs.gitlab.com](https://docs.gitlab.com) (offering multiple versions of all products’ documentation) and at the `/help/` path on each GitLab instance’s domain, with content for that instance’s version and edition.

It is GitLab’s goal to create documentation that is complete, accurate, and easy to use. It should be easy to browse or search for the information you need, and easy to contribute to the documentation itself.

## On this page
{:.no_toc}

- TOC
{:toc}

## Documentation as Single Source of Truth (SSOT)

The documentation is the SSOT for all information related to the implementation, usage, and troubleshooting of GitLab products and features. It evolves continually in keeping with new products and features, and with improvements for clarity, accuracy, and completeness.

This policy prevents information silos, ensuring that it remains easy to find information about GitLab products.

It also informs decisions about the kinds of content we include in our documentation.

### Content we include

As the single source of truth, we include any and all information that may be helpful to the aforementioned audiences and goals.

* This may include (or may link to) helpful content that was not created specifically
for the documentation, including presentations, diagrams, videos, etc.

* We do not withhold workarounds for a single release/time period/niche case, or code that could be construed as dangerous to run, provided that we offer fully detailed warnings and context alongside it. This kind of content should be included in the docs for applicable products/versions, as it could be helpful to others and, when properly explained, its benefits outweigh the risks. If you think you have found an exception to this rule, contact the Technical Writing team.

For detailed standards, see the [Content](https://docs.gitlab.com/ee/development/documentation/styleguide.html#content) section in the [Documentation Style Guide](https://docs.gitlab.com/ee/development/documentation/styleguide.html).

### Content organization

We organize content by topic, not by type, so that it can be located as easily as possible within the "single source of truth" section for the subject matter.

For detailed standards, see [Documentation types and organization](https://docs.gitlab.com/ee/development/documentation/#documentation-types-and-organization) within the [GitLab Documentation guidelines](https://docs.gitlab.com/ee/development/documentation/).

## Docs-first methodology

We employ a **docs-first methodology** to help ensure that the docs remain a complete and trusted resource, and to make communicating about the use of GitLab more efficient.

* If the answer to a question exists in documentation, share the link to the docs instead of rephrasing the information.
* When you encounter new information not available in GitLab’s documentation (for example, when working on a support case or testing a feature), your first step should be to create a merge request to add this information to the docs. You can then share the MR in order to communicate this information. 

New information that would be useful toward the future usage or troubleshooting of GitLab should not be written directly in a forum or other messaging system, but added to a docs MR and then referenced, as described above. Note that among any other doc changes, you can always add a Troubleshooting section to a doc if none exists, or un-comment and use the placeholder Troubleshooting section included as part of our [doc template](https://docs.gitlab.com/ee/development/documentation/structure.html#documentation-template-for-new-docs), if present

The more we reflexively add useful information to the docs, the more (and more successfully) the docs will be used to efficiently accomplish tasks and solve problems.

If you have questions when considering, authoring, or editing docs, ask the Technical Writing team on Slack in `#docs` or in GitLab by mentioning the writer for the applicable [DevOps stage](https://about.gitlab.com/handbook/product/categories/#devops-stages). Otherwise, forge ahead with your best effort. It does not need to be perfect; the team is happy to review and improve upon your content. Please review the [documentation process](https://docs.gitlab.com/ee/development/documentation/) before you begin your first documentation MR.

## Who contributes to the documentation

Everyone can contribute. For documentation needs resulting from the development
and release of new or enhanced features, the developer responsible for the code
writes or updates the docs.

Technical writers monitor the planning and merging of documentation, reviewing all changes after they are merged, unless they are brought in to the process earlier for specific questions, reviews, or projects.

For more information on these processes, see the [Documentation section of our Development docs](https://docs.gitlab.com/ee/development/documentation/).

## Who merges the documentation and when

Anyone with master access at GitLab is welcome to merge documentation changes to GitLab's master branches,
provided they believe the content is:

- clear and sufficiently easy to understand for the intended audience
- technically accurate (per their own knowledge or trust in the author or SME reviewers)
- in line with GitLab's [Documentation Guidelines](https://docs.gitlab.com/ee/development/documentation/) and [Style Guide](https://docs.gitlab.com/ee/development/documentation/styleguide.html)

GitLab's technical writers review all merged content to confirm it is clear, and meets the structure/style guidelines, often making further improvements.

Note that documentation does not need to be perfect in structure and style to merge.
It is better to publish an improved doc (simply better than what's
currently there) and later refine it than to hold the doc in a review process
where users do not know it exists or are using an inferior version of the doc.

However, if you know or suspect that a doc has been merged while in need of further
enhancement, please create another MR or issue for this work.

For more information, see the [Documentation section of our Development docs](https://docs.gitlab.com/ee/development/documentation/).

## Resources about GitLab documentation

- The [Documentation Guidelines](https://docs.gitlab.com/ee/development/documentation/), including pages on:
   - [Documentation workflow](https://docs.gitlab.com/ee/development/documentation/workflow.html)
   - [Documentation page structure and template](https://docs.gitlab.com/ee/development/documentation/structure.html)
   - [Documentation Style Guide](https://docs.gitlab.com/ee/development/documentation/styleguide.html)
   - [Documentaion site architecture](https://docs.gitlab.com/ee/development/documentation/site_architecture/index.html)
- The [Documentation Markdown Guide](https://about.gitlab.com/handbook/product/technical-writing/markdown-guide/)
- The [GitLab Docs project](https://gitlab.com/gitlab-com/gitlab-docs/) which contains the code that pulls the documentation content from multiple repositories and builds docs.gitlab.com

## The Technical Writing team

For more information on the team and how it works to continually improve GitLab docs, see
[Technical Writing](../product/technical-writing/) in the Product section of the handbook.
